/*
 * AsynchActionsManager.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */


#include "AsynchActionsManager.h"

ActionsFlags Flags = {0};

void AsynchActionsHandler(void)
{

	if(Flags.ThermistorFlag)
	{
		// TAKE SOME ACTION
		Flags.ThermistorFlag = false;  // always reset flag at the end of action

	}

}
