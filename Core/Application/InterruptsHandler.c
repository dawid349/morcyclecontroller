/*
 * InterruptsHandler.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#include "InterruptsHandler.h"

//_____________________________________________________

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

//_____________________________________________________

int debug=0;

int time1=0;  // delay testing variables
int time2=0;
int delta_t=7;
extern volatile uint8_t message[8];		// DEBUG ONLY


//uint8_t answer=8;
/* TIME CONTROL STRUCTURE */
DelayTicks ActionsDelay={1,1,1,1}; // instance of struct uses for time control (should by filled only by "1")

/* CAN COMMUNICATION VARIABLES*/
CAN_RxHeaderTypeDef MyRxHeader;

volatile CanMsg CanBusData = {0}; // can data instance

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){


	 if(htim->Instance == TIM7)
		 {
		 	 debug++;
		 	 TimeControl(&ActionsDelay);
		 }


}

/*void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {


	if(huart->Instance == USART1)
	{

	 }
	if(huart->Instance == USART2)
	{

		HAL_UART_Receive_IT(&huart2,rxBuf, 5);

	}
} //end of UART callback function
*/

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	time1=HAL_GetTick();

	while(HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &MyRxHeader, CanBusData.RxData)!=HAL_OK){};

	//FrameID = MyRxHeader.StdId;
	//CanReceiveData(FrameID, &CanBusData);

	time2=HAL_GetTick();
	delta_t=time2-time1;

}//end of CAN callback function
