/*
 * InterruptsHandler.h
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_INTERRUPTSHANDLER_H_
#define APPLICATION_INTERRUPTSHANDLER_H_

#include "TimeManager.h"
#include "can.h"
#include "CanApp.h"
#include "stm32l4xx_hal.h"


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

#endif /* APPLICATION_INTERRUPTSHANDLER_H_ */
